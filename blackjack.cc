// blackjack.cc

#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Implement this function.
int BlackJack(const vector<string>& cards);

int main() {
  while (true) {
    vector<string> cards;
    int num_cards;
    cin >> num_cards;
    if ((cin.rdstate() & istream::failbit) != 0) break;
    cards.resize(num_cards);
    for (int i = 0; i < num_cards; ++i) cin >> cards[i];

    int ret = BlackJack(cards);
    if (ret < 0) break;
    else if (ret == 0) cout << "Exceed" << endl;
    else if (ret == 21) cout << "BlackJack" << endl;
    else cout << ret << endl;
  }
  return 0;
}
