// vending_machine.h

#ifndef _VENDING_MACHINE_H_
#define _VENDING_MACHINE_H_

#include <map>
#include <string>

struct BeverageInfo {
  std::string name;
  int unit_price;
  int stock;
};

class VendingMachine {
 public:
  VendingMachine();

  void AddBeverage(const std::string& name, int unit_price, int stock);
  int Buy(const std::map<std::string, int>& items, int money);

  typedef std::map<std::string, BeverageInfo>::const_iterator const_iterator;
  const_iterator begin() const { return beverages_.begin(); }
  const_iterator end() const { return beverages_.end(); }

 private:
  std::map<std::string, BeverageInfo> beverages_;
};

#endif  // _VENDING_MACHINE_H_
